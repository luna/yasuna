const std = @import("std");
usingnamespace @import("helpers.zig");

pub export fn yn_message_from_json(data: *c.json_t) ?*yn_message_t {
    var msg: *yn_message_t = std.heap.c_allocator.create(yn_message_t) catch |err| {
        std.debug.warn("failed to allocate message: {}\n", .{err});
        return null;
    };

    msg.id = json_get_str(data, "id");
    msg.channel_id = json_get_str(data, "channel_id");
    msg.content = json_get_str(data, "content");

    msg.tstamp_raw = json_get_str(data, "timestamp");

    // TODO edited tstamp can be null?
    msg.edited_tstamp_raw = json_get_str(data, "timestamp");

    msg.tts = json_get_bool(data, "tts");
    msg.mention_everyone = json_get_bool(data, "mention_everyone");
    msg.pinned = json_get_bool(data, "pinned");
    msg.@"type" = @intToEnum(c.enum_yn_message_type, @intCast(c_int, json_get_int(data, "type")));
    msg.author_id = json_get_int(data, "author_id");

    return msg;
}

pub export fn yn_user_from_json(data: *c.json_t, ptr: *yn_user_t) void {
    ptr.id = json_get_str(data, "id");
    ptr.username = json_get_str(data, "username");
    ptr.discriminator = json_get_str(data, "discriminator");
    ptr.avatar = json_get_str(data, "avatar");
    ptr.bot = json_get_bool(data, "bot");
}

pub export fn yn_un_guild_from_json(data: *c.json_t, ptr: *yn_unavailable_guild_t) void {
    ptr.id = json_get_str(data, "id");
    ptr.unavailable = json_get_bool(data, "unavailable");

    // TODO assert unavailable is true?
}

pub export fn yn_guild_from_json(data: *c.json_t, ptr: *yn_guild_t) void {
    ptr.id = json_get_str(data, "id");
    ptr.unavailable = json_get_bool(data, "unavailable");

    // TODO assert unavailable is false?
}
