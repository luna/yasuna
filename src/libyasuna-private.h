#ifndef _YASUNA_PRIVATE_H_
#define _YASUNA_PRIVATE_H_
// private functions for libyasuna

#include <libdill.h>
#include <jansson.h>
#include <string.h>

#include "log.h"
#include "yasuna/libyasuna.h"

// happy functions
#define STREQ(a, b) a != NULL && b != NULL && strcmp(a, b) == 0

#define JSON_DUMPS(obj) json_dumps(obj, JSON_COMPACT)
#define JSON_GET_STR(obj, field) json_string_value(json_object_get(obj, field))
#define JSON_GET_BOOL(obj, field) json_boolean_value(json_object_get(obj, field))
#define JSON_GET_INT(obj, field) json_integer_value(json_object_get(obj, field))

#define HAD_ERR(perror_title, message) log_fatal(message); \
    perror(perror_title); \
    return 1;

#define CHK_STATUS(status_val, perror_msg, log_msg) \
    if(status_val != 0) { \
        HAD_ERR(perror_msg, log_msg); \
    }

// the maximum amount of chars to store a session id
// this can be different from server implementation
// to server implementation, but 256 should give us
// a nice enough default.
#define YN_CLIENT_SESSID_MAX 256
#define YN_EVT_HANDLER_NAME_MAX 256

// maximum discord message size: 1 megabyte
#define YN_GATEWAY_MSG_MAX_SIZE (1024 * 1024)

// private values for the library only. they are
// "private" in the sense that we can change those
// at any time and don't provide support for
// users who mess with the internals of the library
typedef struct {
    // the file descriptor for the tcp socket
    // for the gateway.
    int sockfd;

    // the sequence number for resuming purposes.
    unsigned long long int sequence;

    // the handles returned by libdill are then stored
    // in priv so we can close the underlying coroutines
    // when destroying the client
    int hb_handler;
    int ws_loop_handle;

    // a flag defining if we should send a resume
    // or an identify packet while first connecting
    // to the gateway
    bool resume;

    // a flag defining if we should keep running the main loop
    // or return as soon as possible
    bool run_flag;

    // the session id for the websocket connection
    const char *session_id;

    size_t guild_store_used;
} yn_client_internal;

// private event functions
void yn_event_init();

// dt_from_json.c functions
yn_message_t* yn_message_from_json(json_t*);
void yn_user_from_json(json_t*, yn_user_t*);
void yn_un_guild_from_json(json_t*, yn_unavailable_guild_t*);
void yn_guild_from_json(json_t*, yn_guild_t*);

// == gateway op codes and stuff
enum yn_gateway_op_code {
    OP_DISPATCH,
    OP_HEARTBEAT,
    OP_IDENTIFY,
    OP_STATUS_UPDATE,
    OP_VOICE_STATE_UPDATE,
    OP_VOICE_STATE_PING,
    OP_RESUME,
    OP_RECONNECT,
    OP_REQUEST_GUILD_MEMBERS,
    OP_INVALID_SESSION,
    OP_HELLO,
    OP_HEARTBEAT_ACK,
    OP_GUILD_SYNC,
};

enum yn_vgateway_op_code {
    VGW_IDENTIFY,
    VGW_SELECT_PROTOCOL,
    VGW_READY,
    VGW_HEARTBEAT,
    VGW_SESSION_DESCRIPTION,
    VGW_SPEAKING,
    VGW_HEARTBEAT_ACK,
    VGW_RESUME,
    VGW_HELLO,
    VGW_RESUMED,

    VGW_CLIENT_DISCONNECT = 13,
};

// TODO: export this someday?
enum yn_vgateway_close_code {
    VGW_UNKNOWN_OP = 4001,
};


void ws_loop(yn_client *);

// finding and connection to discord gateway
int yn_gw_connect(struct ipaddr *, const char*);
int yn_gw_close(int sockfd);
int yn_gw_find(struct ipaddr *, const char* path, int port);

// == our part of rest
const char* yn_rest_get_gateway(char* base_url);
void yn_post_message(yn_client* client, char* payload_str,
        const char* channel_id);

// == internal client handlers
int yn_intr_client_ready(yn_client*, void*);
int yn_intr_guild_create(yn_client*, void*);

#endif
