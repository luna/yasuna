const std = @import("std");
usingnamespace @import("helpers.zig");

pub export fn yn_intr_client_ready(client: *yn_client, user_data: *c_void) c_int {
    var ready_data = @ptrCast(*c.json_t, @alignCast(@alignOf(c.json_t), user_data));

    var priv = ynPriv(client);
    priv.session_id = json_get_str(ready_data, "session_id");

    // TODO guild storage
    c.yn_user_from_json(c.json_object_get(ready_data, "user"), client.user);

    var ug_array = c.json_object_get(ready_data, "guilds");
    var guild_count = c.json_array_size(ug_array);
    std.debug.warn("ready: {} guilds\n", .{guild_count});

    var guilds = std.heap.c_allocator.alloc(yn_guild_t, guild_count) catch {
        std.debug.warn("failed to allocate guild array\n", .{});
        return 1;
    };
    std.mem.secureZero(yn_guild_t, guilds);
    client.guilds = guilds.ptr;

    var unavailable_guilds = std.heap.c_allocator.alloc(yn_unavailable_guild_t, guild_count) catch {
        std.debug.warn("failed to allocate guild array\n", .{});
        return 1;
    };
    std.mem.secureZero(yn_unavailable_guild_t, unavailable_guilds);
    client.unavailable_guilds = unavailable_guilds.ptr;

    //#define json_array_foreach(array, index, value) \
    //    for(index = 0; \
    //        index < json_array_size(array) && (value = json_array_get(array, index)); \
    //        index++)

    var ug_index: usize = 0;
    while (ug_index < c.json_array_size(ug_array)) : (ug_index += 1) {
        var u_guild = c.json_array_get(ug_array, ug_index);

        var ptr = &unavailable_guilds[ug_index];
        c.yn_un_guild_from_json(u_guild, ptr);
        std.debug.warn("fill unavailable guild: '{}'\n", .{sliceify(ptr.id)});
    }

    return 0;
}

pub export fn yn_intr_guild_create(client: *yn_client, user_data: *c_void) c_int {
    // TODO
    // var data = @ptrCast(c.json_t, user_data);
    return 0;
}
